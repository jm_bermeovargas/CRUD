﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Ejemplo10Septiembre
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        Model1Container db = new Model1Container(); 

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }


        public List<Usuario> listaUsuarios()
        {
            return db.UsuarioConjunto.ToList(); 
        }


        public bool InsertarUsuario(string user, string pass)
        {

            try 
            {
                Usuario us = new Usuario();


                us.Username = user;
                us.Password = pass;
                db.UsuarioConjunto.Add(us);
                db.SaveChanges();

                return true; 
            }
            catch(Exception e)
            {
                return false; 
            }
            
        }


        public bool ModificarUsuario(int id, string user, string pass)
        {
            try
            {
                //Usuario us = new Usuario();
                var usua = db.UsuarioConjunto.Where<Usuario>(usu => usu.Id.Equals(id));

                foreach (var usu in usua)
                {
                    usu.Username = user;
                    usu.Password = pass;
                }


                db.SaveChanges();

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }


        public bool EliminarUsuario(int id)
        {
            var usua = db.UsuarioConjunto.Where<Usuario>(usu => usu.Id.Equals(id));

            foreach (var usu in usua)
            {
                db.UsuarioConjunto.Remove(usu); 
            }

            db.SaveChanges(); 

            return true; 
        }


        public List<Usuario> ConsultarUsuario(string user)
        {
            List<Usuario> lista = new List<Usuario>();

            var usua = db.UsuarioConjunto.Where<Usuario>(usu => usu.Username.Equals(user));
            foreach (Usuario usu in usua)
            {
                lista.Add(usu);
                
            }

            return lista;  
        }
    }
}
