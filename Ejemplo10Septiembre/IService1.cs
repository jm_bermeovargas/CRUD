﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Ejemplo10Septiembre
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        //Lista todos los usuarios de la tabla
        [OperationContract]
        List<Usuario> listaUsuarios();

        //Inserta un usuario
        [OperationContract]
        Boolean InsertarUsuario(string user, string pass);

        //Modifica el nombre y contraseña del usuario con el id que inserte
        [OperationContract]
        Boolean ModificarUsuario(int id, string user, string pass);

        //elimina el usuario con que tenga el Id que ingrese
        [OperationContract]
        Boolean EliminarUsuario(int id);

        //Busca usuarios que tengan el nombre qeu ingrese
        [OperationContract]
        List<Usuario> ConsultarUsuario(string user); 

        // TODO: agregue aquí sus operaciones de servicio
    }


    // Utilice un contrato de datos, como se ilustra en el ejemplo siguiente, para agregar tipos compuestos a las operaciones de servicio.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
